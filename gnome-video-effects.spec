Name:           gnome-video-effects
Version:        0.6.0
Release:        1
Summary:        Collection for GStreamer video effects
License:        GPLv2
URL:            https://wiki.gnome.org/Projects/GnomeVideoEffects
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.6/%{name}-%{version}.tar.xz

Buildarch:      noarch

BuildRequires:  meson gettext

Requires:       frei0r-plugins

%description
This package is a collection of GStreamer effects to be used in different GNOME Modules.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install

%files
%doc AUTHORS NEWS README COPYING
%{_datadir}/pkgconfig/gnome-video-effects.pc
%{_datadir}/gnome-video-effects

%changelog
* Thu Nov 23 2023 lwg <liweiganga@uniontech.com> - 0.6.0-1
- update to version 0.6.0

* Fri May 28 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.5.0-1
- Upgrade to 0.5.0
- Update Version, Release, Source0, BuildRequires
- Update stage 'build', 'install'

* Wed Dec 11 2019 Tianfei <tianfei16@huawei.com> - 0.4.3-5
- Package init

